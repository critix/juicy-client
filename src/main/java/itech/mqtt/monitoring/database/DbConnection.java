package itech.mqtt.monitoring.database;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.UUID;

public class DbConnection {

    private final DbConfig dbConfig;
    private final Connection connection;

    public DbConnection(DbConfig dbConfig) {
        this.dbConfig = dbConfig;
        connection = connect();
    }

    private Connection connect() {
        Connection connection = null;
        System.out.println("Connecting to database: " + dbConfig.getUrl());
        try {
            connection = DriverManager.getConnection(dbConfig.getUrl(), dbConfig.getUser(), dbConfig.getPassword());
            System.out.println("Successfully connected to the PostgreSQL server");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            System.exit(0);
        }
        return connection;
    }

    public void insertState(String machine, int stateType, int state) {
        String sql = "INSERT INTO mqtt.public.state(id, datetime, machine, statetype, state) VALUES (?,?,?,?,?)";
        try (PreparedStatement pstmt = connection.prepareStatement(sql)) {
            pstmt.setString(1, UUID.randomUUID().toString());
            pstmt.setTimestamp(2, Timestamp.valueOf(LocalDateTime.now()));
            pstmt.setString(3, machine);
            pstmt.setInt(4, stateType);
            pstmt.setInt(5, state);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void insertError(String machine, int machineId, int type, String message) {
        String sql = "INSERT INTO mqtt.public.error(id, datetime, machine, machine_id, type, message) VALUES (?,?,?,?,?,?)";
        try (PreparedStatement pstmt = connection.prepareStatement(sql)) {
            pstmt.setString(1, UUID.randomUUID().toString());
            pstmt.setTimestamp(2, Timestamp.valueOf(LocalDateTime.now()));
            pstmt.setString(3, machine);
            pstmt.setInt(4, machineId);
            pstmt.setInt(5, type);
            pstmt.setString(6, message);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void insertMonitoring(String machine, int type) {
        String sql = "INSERT INTO mqtt.public.monitoring(id, datetime, machine, type) VALUES (?,?,?,?)";
        try (PreparedStatement pstmt = connection.prepareStatement(sql)) {
            pstmt.setString(1, UUID.randomUUID().toString());
            pstmt.setTimestamp(2, Timestamp.valueOf(LocalDateTime.now()));
            pstmt.setString(3, machine);
            pstmt.setInt(4, type);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void insertLogType0(String machine, int pumpId, int pumpState, int orderId) {
        String sql = "INSERT INTO mqtt.public.log(id, datetime, machine, pump_id, pump_state, order_id, type) VALUES (?,?,?,?,?,?,?)";
        try (PreparedStatement pstmt = connection.prepareStatement(sql)) {
            pstmt.setString(1, UUID.randomUUID().toString());
            pstmt.setTimestamp(2, Timestamp.valueOf(LocalDateTime.now()));
            pstmt.setString(3, machine);
            pstmt.setInt(4, pumpId);
            pstmt.setInt(5, pumpState);
            pstmt.setInt(6, orderId);
            pstmt.setInt(7, 0);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void insertLogType1(String machine, String message) {
        String sql = "INSERT INTO mqtt.public.log(id, datetime, machine, message, type) VALUES (?,?,?,?,?)";
        try (PreparedStatement pstmt = connection.prepareStatement(sql)) {
            pstmt.setString(1, UUID.randomUUID().toString());
            pstmt.setTimestamp(2, Timestamp.valueOf(LocalDateTime.now()));
            pstmt.setString(3, machine);
            pstmt.setString(4, message);
            pstmt.setInt(5, 1);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void insertLogType2(String machine, double temperature) {
        String sql = "INSERT INTO mqtt.public.log(id, datetime, machine, temperature, type) VALUES (?,?,?,?,?)";
        try (PreparedStatement pstmt = connection.prepareStatement(sql)) {
            pstmt.setString(1, UUID.randomUUID().toString());
            pstmt.setTimestamp(2, Timestamp.valueOf(LocalDateTime.now()));
            pstmt.setString(3, machine);
            pstmt.setDouble(4, temperature);
            pstmt.setInt(5, 2);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void insertLogType3(String machine, String message) {
        String sql = "INSERT INTO mqtt.public.log(id, datetime, machine, message, type) VALUES (?,?,?,?,?)";
        try (PreparedStatement pstmt = connection.prepareStatement(sql)) {
            pstmt.setString(1, UUID.randomUUID().toString());
            pstmt.setTimestamp(2, Timestamp.valueOf(LocalDateTime.now()));
            pstmt.setString(3, machine);
            pstmt.setString(4, message);
            pstmt.setInt(5, 3);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void insertLogType4(String machine, int state, String message) {
        String sql = "INSERT INTO mqtt.public.log(id, datetime, machine, state, message, type) VALUES (?,?,?,?,?,?)";
        try (PreparedStatement pstmt = connection.prepareStatement(sql)) {
            pstmt.setString(1, UUID.randomUUID().toString());
            pstmt.setTimestamp(2, Timestamp.valueOf(LocalDateTime.now()));
            pstmt.setString(3, machine);
            pstmt.setInt(4, state);
            pstmt.setString(5, message);
            pstmt.setInt(6, 4);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void insertLogType5(String machine, int count, String message) {
        String sql = "INSERT INTO mqtt.public.log(id, datetime, machine, count, message, type) VALUES (?,?,?,?,?,?)";
        try (PreparedStatement pstmt = connection.prepareStatement(sql)) {
            pstmt.setString(1, UUID.randomUUID().toString());
            pstmt.setTimestamp(2, Timestamp.valueOf(LocalDateTime.now()));
            pstmt.setString(3, machine);
            pstmt.setInt(4, count);
            pstmt.setString(5, message);
            pstmt.setInt(6, 5);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
