package itech.mqtt.monitoring.database;

public class DbConfig {
    private final String url = "jdbc:postgresql://10.14.209.99:5432/mqtt";
    private final String user = "postgres";
    private final String password = "wasd";

    public String getUrl() {
        return url;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }
}
