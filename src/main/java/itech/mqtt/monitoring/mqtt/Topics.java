package itech.mqtt.monitoring.mqtt;

public class Topics {
    public static final String ASAFT_STATE = Machines.ASAFT + "/state";
    public static final String ASAFT_ERROR = Machines.ASAFT + "/error";
    public static final String ASAFT_MONITORING = Machines.ASAFT + "/monitoring";
    public static final String ASAFT_LOG = Machines.ASAFT + "/log";
    public static final String OSAFT_STATE = Machines.OSAFT + "/state";
    public static final String OSAFT_ERROR = Machines.OSAFT + "/error";
    public static final String OSAFT_MONITORING = Machines.OSAFT + "/monitoring";
    public static final String OSAFT_LOG = Machines.OSAFT + "/log";
}
