package itech.mqtt.monitoring.mqtt;

import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class MqttConnection {

    private MqttConfig mqttConfig;
    private MqttClient mqttClient;
    private final MqttConnectOptions connectOptions = new MqttConnectOptions();


    public MqttConnection(MqttConfig mqttConfig) {
        this.mqttConfig = mqttConfig;
        connectOptions.setCleanSession(true);
        final String broker = mqttConfig.getBroker();
        final String clientId = mqttConfig.getClientId();
        try {
            this.mqttClient = new MqttClient(broker, clientId, new MemoryPersistence());
            System.out.println("Connecting to broker: " + broker);
            mqttClient.connect(connectOptions);
            System.out.println("Connected");
        } catch (MqttException e) {
            printMqttException(e);
        }
    }

    public void publish(String topic, String message) {
        try {
            System.out.println("Publishing message: " + message);
            MqttMessage mqttMessage = new MqttMessage(message.getBytes());
            mqttMessage.setQos(mqttConfig.getQos());
            mqttClient.publish(topic, mqttMessage);
            System.out.println("Message published");
        } catch (MqttException e) {
            printMqttException(e);
        }
    }

    public void subscribe(String topic) {
        try {
            mqttClient.subscribe(topic);
            System.out.println("Subscribed to topic: " + topic);
        } catch (MqttException e) {
            printMqttException(e);
        }
    }

    public void unsubscribe(String topic) {
        try {
            mqttClient.unsubscribe(topic);
            System.out.println("Unsubscribed from topic: " + topic);
        } catch (MqttException e) {
            printMqttException(e);
        }
    }

    public void disconnect() {
        try {
            mqttClient.disconnect();
            System.out.println("Disconnected");
            System.exit(0);
        } catch (MqttException e) {
            printMqttException(e);
        }
    }

    private void printMqttException(MqttException e) {
        System.out.println("reason " + e.getReasonCode());
        System.out.println("msg " + e.getMessage());
        System.out.println("loc " + e.getLocalizedMessage());
        System.out.println("cause " + e.getCause());
        System.out.println("excep " + e);
        e.printStackTrace();
    }

    public void setMessageHandler(MqttCallback mqttCallback) {
        mqttClient.setCallback(mqttCallback);
    }
}