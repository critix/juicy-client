package itech.mqtt.monitoring.mqtt;

import org.eclipse.paho.client.mqttv3.MqttClient;

public class MqttConfig {
    private String broker = "tcp://10.14.209.99:1883";
    private String clientId = "JuicyMonitor";
    private String username = "mqtt";
    private String password = "mqtt";
    private int qos = 2;

    public String getBroker() {
        return broker;
    }

    public void setBroker(String broker) {
        this.broker = broker;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getQos() {
        return qos;
    }

    public void setQos(int qos) {
        this.qos = qos;
    }
}
