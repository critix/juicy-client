package itech.mqtt.monitoring.client;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.*;
import javafx.stage.Stage;

import java.io.IOException;

public class Client extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("monitor.fxml"));
        Scene scene = new Scene(root);
        primaryStage.setTitle("Juicy Monitoring CLient");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}