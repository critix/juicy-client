package itech.mqtt.monitoring.client;

import itech.mqtt.monitoring.database.DbConfig;
import itech.mqtt.monitoring.database.DbConnection;
import itech.mqtt.monitoring.mqtt.Machines;
import itech.mqtt.monitoring.mqtt.MqttConfig;
import itech.mqtt.monitoring.mqtt.MqttConnection;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import org.json.JSONObject;

import java.util.*;

public class MonitorController {

    private final MqttConnection mqttConnection;
    private final DbConnection dbConnection;
    private final MonitorMessageHandler monitorMessageHandler;
    public ToggleButton haltAsaft;
    public ToggleButton stopHaltAsaft;
    public Label stateMessageAsaft;
    public Label stateMessageOsaft;
    public Rectangle pump1Fill1Osaft;
    public Rectangle pump1Fill2Osaft;
    public Rectangle pump1Fill3Osaft;
    public Rectangle pump2Fill1Osaft;
    public Rectangle pump2Fill2Osaft;
    public Rectangle pump2Fill3Osaft;
    public Rectangle pump3Fill1Osaft;
    public Rectangle pump3Fill2Osaft;
    public Rectangle pump3Fill3Osaft;
    public Rectangle pump4Fill1Osaft;
    public Rectangle pump4Fill2Osaft;
    public Rectangle pump4Fill3Osaft;
    public Rectangle pump1Fill1Asaft;
    public Rectangle pump1Fill2Asaft;
    public Rectangle pump1Fill3Asaft;
    public Rectangle pump2Fill1Asaft;
    public Rectangle pump2Fill2Asaft;
    public Rectangle pump2Fill3Asaft;
    public Rectangle pump3Fill1Asaft;
    public Rectangle pump3Fill2Asaft;
    public Rectangle pump3Fill3Asaft;
    public Rectangle pump4Fill1Asaft;
    public Rectangle pump4Fill2Asaft;
    public Rectangle pump4Fill3Asaft;
    public Map<Integer, List<Rectangle>> pumpsOsaft = new HashMap<>();
    public Map<Integer, List<Rectangle>> pumpsAsaft = new HashMap<>();
    public Label pump1MessageOsaft;
    public Label pump2MessageOsaft;
    public Label pump3MessageOsaft;
    public Label pump4MessageOsaft;
    public Label pump1MessageAsaft;
    public Label pump2MessageAsaft;
    public Label pump3MessageAsaft;
    public Label pump4MessageAsaft;
    public Map<Integer, Label> pumpsMessagesAsaft = new HashMap<>();
    public Map<Integer, Label> pumpsMessagesOsaft = new HashMap<>();
    public Map<Integer, Circle> pumpCirclesAsaft = new HashMap<>();
    public Map<Integer, Circle> pumpCirclesOsaft = new HashMap<>();
    public Circle pump1CircleOsaft;
    public Circle pump2CircleOsaft;
    public Circle pump3CircleOsaft;
    public Circle pump4CircleOsaft;
    public Circle pump1CircleAsaft;
    public Circle pump2CircleAsaft;
    public Circle pump3CircleAsaft;
    public Circle pump4CircleAsaft;
    public ToggleButton haltOsaft;
    public ToggleButton stopHaltOsaft;
    public Label tempOsaft;
    public Label tempAsaft;
    public Label countAsaft;
    public Label countOsaft;

    public MonitorController() {
        MqttConfig mqttConfig = new MqttConfig();
        mqttConnection = new MqttConnection(mqttConfig);
        monitorMessageHandler = new MonitorMessageHandler(this);
        mqttConnection.setMessageHandler(monitorMessageHandler);
        DbConfig dbConfig = new DbConfig();
        this.dbConnection = new DbConnection(dbConfig);
        mqttConnection.subscribe(Machines.ASAFT + "/#");
        mqttConnection.subscribe(Machines.OSAFT + "/#");
    }

    private void initPumpMap() {
        pumpsAsaft.clear();
        pumpsOsaft.clear();
        List<Rectangle> pumps1Osaft = Arrays.asList(pump1Fill1Osaft, pump1Fill2Osaft, pump1Fill3Osaft);
        List<Rectangle> pumps2Osaft = Arrays.asList(pump2Fill1Osaft, pump2Fill2Osaft, pump2Fill3Osaft);
        List<Rectangle> pumps3Osaft = Arrays.asList(pump3Fill1Osaft, pump3Fill2Osaft, pump3Fill3Osaft);
        List<Rectangle> pumps4Osaft = Arrays.asList(pump4Fill1Osaft, pump4Fill2Osaft, pump4Fill3Osaft);
        List<Rectangle> pumps1Asaft = Arrays.asList(pump1Fill1Asaft, pump1Fill2Asaft, pump1Fill3Asaft);
        List<Rectangle> pumps2Asaft = Arrays.asList(pump2Fill1Asaft, pump2Fill2Asaft, pump2Fill3Asaft);
        List<Rectangle> pumps3Asaft = Arrays.asList(pump3Fill1Asaft, pump3Fill2Asaft, pump3Fill3Asaft);
        List<Rectangle> pumps4Asaft = Arrays.asList(pump4Fill1Asaft, pump4Fill2Asaft, pump4Fill3Asaft);
        pumpsOsaft.put(1, pumps1Osaft);
        pumpsOsaft.put(2, pumps2Osaft);
        pumpsOsaft.put(3, pumps3Osaft);
        pumpsOsaft.put(4, pumps4Osaft);
        pumpsAsaft.put(1, pumps1Asaft);
        pumpsAsaft.put(2, pumps2Asaft);
        pumpsAsaft.put(3, pumps3Asaft);
        pumpsAsaft.put(4, pumps4Asaft);
    }

    private void initPumpMessageLabelsAndCircles() {
        pumpsMessagesAsaft.clear();
        pumpsMessagesAsaft.clear();
        pumpsMessagesAsaft.put(1, pump1MessageAsaft);
        pumpsMessagesAsaft.put(2, pump2MessageAsaft);
        pumpsMessagesAsaft.put(3, pump3MessageAsaft);
        pumpsMessagesAsaft.put(4, pump4MessageAsaft);
        pumpsMessagesOsaft.put(1, pump1MessageOsaft);
        pumpsMessagesOsaft.put(2, pump2MessageOsaft);
        pumpsMessagesOsaft.put(3, pump3MessageOsaft);
        pumpsMessagesOsaft.put(4, pump4MessageOsaft);

        pumpCirclesAsaft.put(1, pump1CircleAsaft);
        pumpCirclesAsaft.put(2, pump2CircleAsaft);
        pumpCirclesAsaft.put(3, pump3CircleAsaft);
        pumpCirclesAsaft.put(4, pump4CircleAsaft);

        pumpCirclesOsaft.put(1, pump1CircleOsaft);
        pumpCirclesOsaft.put(2, pump2CircleOsaft);
        pumpCirclesOsaft.put(3, pump3CircleOsaft);
        pumpCirclesOsaft.put(4, pump4CircleOsaft);
    }

    public void onError(String machine, int id, int type, String message) {
        Platform.runLater(() -> {
            switch (machine) {
                case Machines.OSAFT:
                    stateMessageOsaft.setText(message);
                    break;
                case Machines.ASAFT:
                    stateMessageAsaft.setText(message);
                    break;
            }
        });
        dbConnection.insertError(machine, id, type, message);
    }

    public void onState(String machine, int stateType, int state) {
        switch (stateType) {
            case 0:
                onMachineState(machine, state);
                break;
            case 1:
                onFillState(machine, state);
                break;
        }
        dbConnection.insertState(machine, stateType, state);
    }

    private void onFillState(String machine, int state) {
        String message;
        switch (state) {
            case 0:
                message = "Niveauschalter oben";
                break;
            case 1:
                message = "Niveauschalter mitte";
                break;
            case 2:
                message = "Niveauschalter unten";
                break;
            default:
                message = "ERROR: unknown state " + state;
        }
        updateState(machine, message);
        updateFillState(machine, state);
    }

    private void updateFillState(String machine, int state) {
        Platform.runLater(() -> {
            initPumpMap();
            String fillColor = "#3c87ff";
            final int invertedState = state == 2 ? 0 : state == 0 ? 2 : 1;
            switch (machine) {
                case Machines.ASAFT:
                    // reset
                    pumpsAsaft.forEach((integer, rectangles) -> rectangles.forEach(rectangle -> rectangle.setFill(Color.WHITE)));
                    // fill
                    pumpsAsaft.forEach((integer, rectangles) -> {
                        for (int i = 0; i <= invertedState; i++) {
                            rectangles.get(i).setFill(Color.valueOf(fillColor));
                        }
                    });
                    break;
                case Machines.OSAFT:
                    // reset
                    pumpsOsaft.forEach((integer, rectangles) -> rectangles.forEach(rectangle -> rectangle.setFill(Color.WHITE)));
                    // fill
                    pumpsOsaft.forEach((integer, rectangles) -> {
                        for (int i = 0; i <= invertedState; i++) {
                            rectangles.get(i).setFill(Color.valueOf(fillColor));
                        }
                    });
                    break;
            }
        });
    }

    private void updateState(String machine, String message) {
        Platform.runLater(() -> {
            switch (machine) {
                case Machines.ASAFT:
                    stateMessageAsaft.setText(message);
                    break;
                case Machines.OSAFT:
                    stateMessageOsaft.setText(message);
                    break;
            }
        });
    }

    private void onMachineState(String machine, int state) {
        String message;
        switch (state) {
            case 0:
                message = "Die Maschine ist betriebsbereit";
                break;
            case 1:
                message = "Die Maschine pumpt";
                break;
            case 2:
                message = "Die Maschine ist im Fertigungsprozess";
                break;
            default:
                message = "ERROR: unknown state " + state;
        }
        updateState(machine, message);
    }

    public void onMonitoring(String machine, int type) {
        dbConnection.insertMonitoring(machine, type);
    }

    public void onLogType0(String machine, int pumpId, int pumpState, int orderId) {
        Platform.runLater(() -> {
            initPumpMessageLabelsAndCircles();
            String message;
            String color;
            switch (pumpState) {
                case 0:
                    message = "Betriebsbereit";
                    color = "#05a100";
                    break;
                case 1:
                    message = "Pumpt";
                    color = "#bc0000";
                    break;
                default:
                    message = "ERROR: unknown pump-state " + pumpState;
                    color = "000000";
            }
            switch (machine) {
                case Machines.ASAFT:
                    pumpsMessagesAsaft.get(pumpId).setText(message);
                    pumpCirclesAsaft.get(pumpId).setStroke(Color.valueOf(color));
                    break;
                case Machines.OSAFT:
                    pumpsMessagesOsaft.get(pumpId).setText(message);
                    pumpCirclesOsaft.get(pumpId).setStroke(Color.valueOf(color));
                    break;
            }
        });
        dbConnection.insertLogType0(machine, pumpId, pumpState, orderId);
    }

    public void onLogType1(String machine, String message) {
        Platform.runLater(() -> {
            switch (machine) {
                case Machines.ASAFT:
                    stateMessageAsaft.setText(message);
                    break;
                case Machines.OSAFT:
                    stateMessageOsaft.setText(message);
                    break;
            }
        });
        dbConnection.insertLogType1(machine, message);
    }

    public void onLogType2(String machine, double temperature) {
        Platform.runLater(() -> {
            switch (machine) {
                case Machines.ASAFT:
                    tempAsaft.setText(String.format("%.1f°C", temperature));
                    break;
                case Machines.OSAFT:
                    tempOsaft.setText(String.format("%.1f°C", temperature));
                    break;
            }
        });
        dbConnection.insertLogType2(machine, temperature);
    }

    public void onLogType3(String machine, String message) {
        // TODO implement GUI
        dbConnection.insertLogType3(machine, message);
    }

    public void onLogType4(String machine, int state, String message) {
        updateState(machine, message);
        dbConnection.insertLogType4(machine, state, message);
    }

    public void onLogType5(String machine, int count, String message) {
        Platform.runLater(() -> {
            switch (machine) {
                case Machines.ASAFT:
                    countAsaft.setText(String.valueOf(count));
                    break;
                case Machines.OSAFT:
                    countOsaft.setText(String.valueOf(count));
                    break;
            }
        });
        dbConnection.insertLogType5(machine, count, message);
    }

    private void onHalt(String machine, int type) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("type", type);
        mqttConnection.publish(machine + "/monitoring", jsonObject.toString());
    }

    public void onHaltAsaft(ActionEvent actionEvent) {
        onHalt(Machines.ASAFT, 0);
        Platform.runLater(() -> {
            haltAsaft.setBackground(new Background(new BackgroundFill(Paint.valueOf("#05a100"), null, null)));
            stopHaltAsaft.setBackground(new Background(new BackgroundFill(Color.WHITE, null, null)));
            stopHaltAsaft.setText("OFF");
            haltAsaft.setText("");
        });

    }

    public void onStopHaltAsaft(ActionEvent actionEvent) {
        onHalt(Machines.ASAFT, 1);
        Platform.runLater(() -> {
            haltAsaft.setBackground(new Background(new BackgroundFill(Color.WHITE, null, null)));
            stopHaltAsaft.setBackground(new Background(new BackgroundFill(Paint.valueOf("#bc0000"), null, null)));
        stopHaltAsaft.setText("");
        haltAsaft.setText("ON");
        });


    }

    public void onHaltOsaft(ActionEvent actionEvent) {
        onHalt(Machines.OSAFT, 0);
        Platform.runLater(() -> {
            haltOsaft.setBackground(new Background(new BackgroundFill(Paint.valueOf("#05a100"), null, null)));
            stopHaltOsaft.setBackground(new Background(new BackgroundFill(Color.WHITE, null, null)));
            stopHaltOsaft.setText("OFF");
            haltOsaft.setText("");
        });
    }

    public void onStopHaltOsaft(ActionEvent actionEvent) {
        onHalt(Machines.OSAFT, 1);
        Platform.runLater(() -> {
            haltOsaft.setBackground(new Background(new BackgroundFill(Color.WHITE, null, null)));
            stopHaltOsaft.setBackground(new Background(new BackgroundFill(Paint.valueOf("#bc0000"), null, null)));
            stopHaltOsaft.setText("");
            haltOsaft.setText("ON");
        });
    }

    public void setPumpStateToPumping() {

    }
}
