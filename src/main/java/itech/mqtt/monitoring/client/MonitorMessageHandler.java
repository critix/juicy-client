package itech.mqtt.monitoring.client;

import itech.mqtt.monitoring.mqtt.Machines;
import itech.mqtt.monitoring.mqtt.Topics;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;
import org.json.JSONObject;

public class MonitorMessageHandler implements MqttCallback {

    private final MonitorController monitorController;

    public MonitorMessageHandler(MonitorController monitorController) {
        this.monitorController = monitorController;
    }

    public void connectionLost(Throwable cause) {
        System.out.println("Connection lost");
        System.out.println(cause.getMessage());
    }

    public void messageArrived(String topic, MqttMessage message) throws Exception {
        System.out.println(String.format("%s %s", topic, message));
        String messageContent = message.toString();
        switch (topic) {
            case Topics.OSAFT_STATE:
                handleStateMessage(Machines.OSAFT, messageContent);
                break;
            case Topics.OSAFT_ERROR:
                handleErrorMessage(Machines.OSAFT, messageContent);
                break;
            case Topics.OSAFT_MONITORING:
                handleMonitoringMessage(Machines.OSAFT, messageContent);
                break;
            case Topics.OSAFT_LOG:
                handleLogMessage(Machines.OSAFT, messageContent);
                break;
            case Topics.ASAFT_STATE:
                handleStateMessage(Machines.ASAFT, messageContent);
                break;
            case Topics.ASAFT_ERROR:
                handleErrorMessage(Machines.ASAFT, messageContent);
                break;
            case Topics.ASAFT_MONITORING:
                handleMonitoringMessage(Machines.ASAFT, messageContent);
                break;
            case Topics.ASAFT_LOG:
                handleLogMessage(Machines.ASAFT, messageContent);
                break;
            default:
                System.out.println(String.format("Message for unsupported topic: %s %s", topic, messageContent));
        }
    }

    private void handleLogMessage(String machine, String logMessage) {
        try {
            JSONObject logJson = new JSONObject(logMessage);
            int type = logJson.getInt("type");
            switch (type) {
                case 0: {
                    int pumpId = logJson.getJSONObject("message").getJSONObject("pumpe").getInt("id");
                    int pumpState = logJson.getJSONObject("message").getJSONObject("pumpe").getInt("state");
                    int orderId = logJson.getJSONObject("message").getJSONObject("pumpe").getInt("orderId");
                    monitorController.onLogType0(machine, pumpId, pumpState, orderId);
                    break;
                }
                case 1: {
                    String message = logJson.getString("message");
                    monitorController.onLogType1(machine, message);
                    break;
                }
                case 2: {
                    double temperature = logJson.getJSONObject("message").getDouble("temperature");
                    monitorController.onLogType2(machine, temperature);
                    break;
                }
                case 3: {
                    String message = logJson.getString("message");
                    monitorController.onLogType3(machine, message);
                    break;
                }
                case 4: {
                    int state = logJson.getJSONObject("message").getInt("state");
                    String message= logJson.getJSONObject("message").getString("message");
                    monitorController.onLogType4(machine, state, message);
                    break;
                }
                case 5: {
                    int count = logJson.getJSONObject("message").getInt("count");
                    String message= logJson.getJSONObject("message").getString("message");
                    monitorController.onLogType5(machine, count, message);
                    break;
                }
                default:
                    System.out.println(String.format("Unsupported type (%d) for log message: %s", type, logMessage));
            }
        } catch (JSONException e) {
            System.out.println("Error parsing JSON message: " + logMessage);
        }
    }

    private void handleMonitoringMessage(String machine, String monitoringMessage) {
        try {
            JSONObject monitoringJson = new JSONObject(monitoringMessage);
            int type = monitoringJson.getInt("type");
            monitorController.onMonitoring(machine, type);
        } catch (JSONException e) {
            System.out.println("Error parsing JSON message: " + monitoringMessage);
        }
    }

    private void handleErrorMessage(String machine, String errorMessage) {
        try {
            JSONObject errorJson = new JSONObject(errorMessage);
            int type = errorJson.getJSONObject("machine").getInt("type");
            int id = errorJson.getJSONObject("machine").getInt("id");
            String message = errorJson.getString("message");
            monitorController.onError(machine, id, type, message);
        } catch (JSONException e) {
            System.out.println("Error parsing JSON message: " + errorMessage);
        }
    }

    private void handleStateMessage(String machine, String stateMessage) {
        try {
            JSONObject stateJson = new JSONObject(stateMessage);
            int stateType = stateJson.getInt("stateType");
            int state = stateJson.getInt("state");
            monitorController.onState(machine, stateType, state);
        } catch (JSONException e) {
            System.out.println("Error parsing JSON message: " + stateMessage);
        }
    }

    public void deliveryComplete(IMqttDeliveryToken token) {
        System.out.println("Message was successfully delivered");
    }
}
