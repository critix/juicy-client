create database mqtt
	with owner postgres;

create table if not exists state
(
	id varchar not null
		constraint state_pk
			primary key,
	datetime timestamp not null,
	statetype integer not null,
	state integer not null,
	machine varchar not null
);

alter table state owner to postgres;

create table if not exists monitoring
(
	id varchar not null
		constraint monitoring_pk
			primary key,
	datetime timestamp not null,
	machine varchar not null,
	type integer not null
);

alter table monitoring owner to postgres;

create table if not exists log
(
	datetime timestamp not null,
	type integer not null,
	id varchar not null
		constraint log_pk
			primary key,
	machine varchar not null,
	pump_id integer,
	pump_state integer,
	order_id integer,
	message varchar,
	temperature double precision,
	state integer,
	count integer
);

alter table log owner to postgres;

create unique index if not exists log_id_uindex
	on log (id);

create table if not exists error
(
	id varchar not null
		constraint error_pk
			primary key,
	datetime timestamp not null,
	machine_id integer not null,
	type integer,
	machine varchar not null,
	message varchar
);

alter table error owner to postgres;

create unique index if not exists error_id_uindex
	on error (id);